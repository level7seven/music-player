import 'package:flutter/cupertino.dart';

const Color K_primaryOrange = Color(0xffE16428);
const Color K_accentOrange = Color(0x4fE16428);
const Color K_primaryBlack = Color(0xff272121);
const Color K_accentBlack = Color(0xff363333);
const Color K_transparentWhite = Color(0xf0ffffff);
const Color K_transparentWhite2 = Color(0x2affffff);
const Color K_primaryWhite = Color(0xffF6E9E9);
