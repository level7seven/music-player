enum ResponseStatus { Unauthorized, Success, Fail }

class Api {
  static String apiKey = '';
  static String baseUrl = "api.arvincms.ir/api";

  static String auth = "/user/authenticate";
  static String register = "/usear/register";

  static String channels = "/channels";

  static String channelMusics(String id) => "/channel/musics?_id=$id";

  static String geners = "/genres";

  static String generMusics(String id) => "/genre/musics?_id=$id";

  static String artists = "/artists";

  static String artistMusics(String id) => "/artist/musics?_id=$id";

  static String albums = "/albums";

  static String albumMusics(String id) => "/album/musics?_id=$id";

  static String musics = "/musics";
}
