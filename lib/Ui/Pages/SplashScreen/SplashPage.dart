import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Ui/Components/Loading.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  bool connectedInternet= true;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: K_primaryBlack,
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  width: 125,
                  height: 125,
                  child: SvgPicture.asset(
                    'assets/images/guitar.svg',
                    semanticsLabel: 'app logo ',
                    color: K_primaryOrange,
                    height: 98,
                    width: 98,
                  ),
                ),
              ],
            ),
            new Padding(padding: const EdgeInsets.only(bottom:30)
              ,child:  connectedInternet
                    ?
                new Align(
                  alignment: Alignment.bottomCenter,
                  child: Loading()
              )
                    :
                new Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Directionality(
                          textDirection: TextDirection.rtl,
                            child: Text('خطا در اتصال به اینترنت ...',style: TextStyle(color: Colors.white),)),
                        FlatButton(
                            padding: EdgeInsets.all(0),
                            color: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            onPressed: () {
                              login();
                            },
                            child: Container(
                              width: 100,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  new Icon(Icons.refresh,color: Colors.white,size: 18,),
                                  SizedBox(width: 3,),
                                  new Text('تلاش مجدد',style: TextStyle(color: Colors.white,fontSize: 14),)
                                ],
                              ),
                            )),
                      ],
                    )
                )
            )
          ],
        )
    );
  }

  Future<bool> checkInternetConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());


    return connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi;
  }
  @override
  void initState() {
    super.initState();
   login();
  }

   login()async {

    if( await checkInternetConnection()){
     //
      NetworkUtil.internal().getSongs().then((value) {
        NetworkUtil.internal().getChannels().then((value) {
          NetworkUtil.internal().getGenres().then((value) {
            NetworkUtil.internal().getArtist().then((value) {
               Navigator.pushReplacementNamed(context, '/root');
            });
          });
        });
      });
    }
    else{
      setState(() {
        connectedInternet= false;
      });
    }
   }
}
