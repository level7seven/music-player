import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String fullName;
  String phoneNumber;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fullName = "مهدی شبانی";
    phoneNumber = "09010632947";
  }

  @override
  Widget build(BuildContext context) {
//

    return new Scaffold(
      backgroundColor: K_primaryBlack,
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
            child: new Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Center(
                child: Padding(
                    padding: EdgeInsets.only(
                        top: 2.92 * SizeConfig.heightMultiplier),
                    child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Colors.grey,
                      backgroundImage:
                          new AssetImage("assets/images/pic1 (1).jpeg"),
                    )),
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: 2.92 * SizeConfig.heightMultiplier),
                  child: new Text(
                    fullName,
                    style: TextStyle(
                        fontFamily: "IRANYekanMobile",
                        fontSize: 2.63 * SizeConfig.textMultiplier,
                        color: K_primaryOrange,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: 1.46 * SizeConfig.heightMultiplier),
                  child: new Text(
                    phoneNumber,
                    style: TextStyle(
                        fontFamily: "IRANYekanMobile",
                        fontSize: 2.04 * SizeConfig.heightMultiplier,
                        color: K_primaryWhite,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 5.85 * SizeConfig.heightMultiplier),
                child: new Divider(
                  height: 2.92 * SizeConfig.heightMultiplier,
                  thickness: 2,
                  color: K_accentBlack,
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(top: 5.85 * SizeConfig.heightMultiplier),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: new Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              print(
                                  "=====================================================");
                            },
                            child: new Column(
                              children: [
                                Center(
                                  child: SvgPicture.asset(
                                    'assets/images/playlist.svg',
                                    semanticsLabel: 'app logo ',
                                    color: K_primaryOrange,
                                    height: 30,
                                    width: 30,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        bottom:
                                            10 * SizeConfig.heightMultiplier,
                                        top:
                                            1.46 * SizeConfig.heightMultiplier),
                                    child: new Text(
                                      "پلی لیست های من",
                                      style: TextStyle(
                                          fontFamily: "IRANYekanMobile",
                                          fontSize: 2.04 *
                                              SizeConfig.heightMultiplier,
                                          color: K_primaryWhite,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Column(
                              children: [
                                Center(
                                  child: SvgPicture.asset(
                                    'assets/images/user.svg',
                                    semanticsLabel: 'app logo ',
                                    color: K_primaryOrange,
                                    height: 30,
                                    width: 30,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        bottom:
                                            1.46 * SizeConfig.heightMultiplier,
                                        top:
                                            1.46 * SizeConfig.heightMultiplier),
                                    child: new Text(
                                      "ویرایش پروفایل",
                                      style: TextStyle(
                                          fontFamily: "IRANYekanMobile",
                                          fontSize:
                                              2.04 * SizeConfig.textMultiplier,
                                          color: K_primaryWhite,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    new Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            print(
                                "=====================================================");
                          },
                          child: new Column(
                            children: [
                              Center(
                                child: SvgPicture.asset(
                                  'assets/images/favorite.svg',
                                  semanticsLabel: 'app logo ',
                                  color: K_primaryOrange,
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      bottom: 10 * SizeConfig.heightMultiplier,
                                      top: 1.46 * SizeConfig.heightMultiplier),
                                  child: new Text(
                                    "علاقه مندی ها",
                                    style: TextStyle(
                                        fontFamily: "IRANYekanMobile",
                                        fontSize:
                                            2.04 * SizeConfig.textMultiplier,
                                        color: K_primaryWhite,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            print(
                                "=====================================================");
                          },
                          child: new Column(
                            children: [
                              Center(
                                child: SvgPicture.asset(
                                  'assets/images/logout.svg',
                                  semanticsLabel: 'app logo ',
                                  color: K_primaryOrange,
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      bottom:
                                          1.46 * SizeConfig.heightMultiplier,
                                      top: 1.46 * SizeConfig.heightMultiplier),
                                  child: new Text(
                                    "خروج از حساب کاربری",
                                    style: TextStyle(
                                        fontFamily: "IRANYekanMobile",
                                        fontSize:
                                            2.04 * SizeConfig.textMultiplier,
                                        color: K_primaryWhite,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
