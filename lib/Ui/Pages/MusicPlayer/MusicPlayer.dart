import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';

typedef void OnError(Exception exception);

enum PlayerState { stopped, playing, paused }
enum PlayingRouteState { speakers, earpiece }

double _panelHeightOpen;

double _panelWidthOpen;
bool isPlaying = false;
int songsPlayIndex = 0;

class MusicPlayer extends StatefulWidget {
  ScrollController sc;

  MusicPlayer(this.sc);

  @override
  _MusicPlayerState createState() => new _MusicPlayerState();
}

class _MusicPlayerState extends State<MusicPlayer> {
  String url = songsData[songsPlayIndex].link128;
  PlayerMode mode;

  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;

//  Duration _duration;
//  Duration _position;

  PlayerState _playerState = PlayerState.stopped;
  PlayingRouteState _playingRouteState = PlayingRouteState.speakers;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;

  get _isPlaying => _playerState == PlayerState.playing;

  get _isPaused => _playerState == PlayerState.paused;

  get _durationText => _duration?.toString()?.split('.')?.first ?? '';

  get _positionText => _position?.toString()?.split('.')?.first ?? '';

  get _isPlayingThroughEarpiece =>
      _playingRouteState == PlayingRouteState.earpiece;

  Duration _duration = new Duration();
  Duration _position = new Duration();
  AudioPlayer advancedPlayer;

//  AudioCache audioCache;

  @override
  void initState() {
    super.initState();
    initPlayer();
    _initAudioPlayer();
  }

  void initPlayer() async {
    advancedPlayer = new AudioPlayer();
    AudioPlayer.logEnabled = true;

    advancedPlayer.durationHandler = (d) => setState(() {
          _duration = d;
        });

    advancedPlayer.positionHandler = (p) => setState(() {
          _position = p;
        });
  }

  String localFilePath;

  Widget _tab(List<Widget> children) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: children
              .map((w) => Container(child: w, padding: EdgeInsets.all(6.0)))
              .toList(),
        ),
      ),
    );
  }

  Widget _btn(String txt, VoidCallback onPressed) {
    return ButtonTheme(
        minWidth: 48.0,
        child: RaisedButton(child: Text(txt), onPressed: onPressed));
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    advancedPlayer.seek(newDuration);
  }

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height;
    _panelWidthOpen = MediaQuery.of(context).size.width;

    return DefaultTabController(
      length: 1,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        color: K_primaryBlack,
        child: SingleChildScrollView(
          controller: widget.sc,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: SvgPicture.asset(
                  'assets/images/Line.svg',
                  semanticsLabel: 'minus',
                  color: K_transparentWhite,
                  height: 200,
                  width: 200,
                ),
                onPressed: () {},
              ),
              Container(
                width: _panelWidthOpen * 60 / 100,
                height: _panelWidthOpen * 60 / 100,
                margin: EdgeInsets.only(
                    bottom: _panelHeightOpen / 20, top: _panelWidthOpen / 15),
//                  vertical: _panelWidthOpen / 15,
//                  horizontal: _panelWidthOpen / 5),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(songsData[songsPlayIndex].coverArt),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    _positionText ?? '',
                    style: TextStyle(color: K_primaryWhite, fontSize: 16.0),
                  ),
                  Text(
                    _durationText ?? '',
                    style: TextStyle(color: K_primaryWhite, fontSize: 16.0),
                  ),
                ],
              ),
              slider(),
              Container(
                margin: EdgeInsets.only(top: 32),
                child: Text(
                  songsData[songsPlayIndex].name,
                  style: TextStyle(
                    fontSize: 18,
                    color: K_primaryWhite,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  songsData[songsPlayIndex].singers[0].name,
                  style: TextStyle(
                    fontSize: 12,
                    color: K_primaryWhite,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Material(
                    color: Colors.transparent,
                    child: IconButton(
                      icon: SvgPicture.asset(
                        'assets/images/like.svg',
                        semanticsLabel: 'like',
                        color: K_transparentWhite,
                        height: 24,
                        width: 24,
                      ),
                      onPressed: () {},
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 54,
                        height: 54,
                        child: FlatButton(
                            padding: EdgeInsets.all(0),
                            color: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(90.0),
                            ),
                            onPressed: () {
                              _position = Duration(seconds: 0);
                              _stop();
                              int length = songsData.length;
                              if (length == songsPlayIndex + 1) {
                                setState(() {
                                  songsPlayIndex = 0;
                                  url = songsData[songsPlayIndex].link128;
                                });
                                _play();
                              } else {
                                setState(() {
                                  songsPlayIndex++;
                                  url = songsData[songsPlayIndex].link128;
                                });
                                _play();
                              }
                            },
                            child: Container(
                              height: 16,
                              width: 16,
                              child: SvgPicture.asset(
                                'assets/images/previous.svg',
                                semanticsLabel: 'pause',
                                color: K_transparentWhite,
                              ),
                            )),
                      ),
                      SizedBox(
                        width: 54,
                        height: 54,
                        child: FlatButton(
                            padding: EdgeInsets.all(0),
                            color: K_accentBlack,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(90.0),
                            ),
                            onPressed: () {
                              if (!isPlaying) {
                                _play();
                              } else {
                                _pause();
                              }
                            },
                            child: isPlaying
                                ? Container(
                                    height: 24,
                                    width: 24,
                                    child: SvgPicture.asset(
                                      'assets/images/pause.svg',
                                      semanticsLabel: 'pause',
                                      color: K_transparentWhite,
                                    ),
                                  )
                                : Padding(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Container(
                                      height: 22,
                                      width: 22,
                                      child: SvgPicture.asset(
                                        'assets/images/play.svg',
                                        semanticsLabel: 'pause',
                                        color: K_transparentWhite,
                                      ),
                                    ),
                                  )
//
//
//
//
//
//                            ,
                            ),
                      ),
                      SizedBox(
                        width: 54,
                        height: 54,
                        child: FlatButton(
                            padding: EdgeInsets.all(0),
                            color: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(90.0),
                            ),
                            onPressed: () {
                              _position = Duration(seconds: 0);
                              _stop();
                              int length = songsData.length;
                              if (length == songsPlayIndex + 1) {
                                setState(() {
                                  songsPlayIndex = 0;
                                  url = songsData[songsPlayIndex].link128;
                                });
                                _play();
                              } else {
                                setState(() {
                                  songsPlayIndex++;
                                  url = songsData[songsPlayIndex].link128;
                                });
                                _play();
                              }
                            },
                            child: Container(
                              height: 16,
                              width: 16,
                              child: SvgPicture.asset(
                                'assets/images/next.svg',
                                semanticsLabel: 'pause',
                                color: K_transparentWhite,
                              ),
                            )),
                      ),
                    ],
                  ),
                  Material(
                    color: Colors.transparent,
                    child: IconButton(
                      icon: Icon(
                        Icons.bookmark_border,
                        size: 36,
                        color: K_transparentWhite2,
                      ),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
              Divider(
                height: 16,
                color: K_transparentWhite2,
              ),
              Container(
                width: double.infinity,
                height: 48,
                margin: EdgeInsets.symmetric(vertical: 16),
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                child: MaterialButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'تصادفی',
                        style: TextStyle(fontSize: 16, color: K_primaryOrange),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      SvgPicture.asset(
                        'assets/images/random.svg',
                        semanticsLabel: 'random',
                        color: K_primaryOrange,
                        height: 14,
                        width: 14,
                      ),
                    ],
                  ),
                  color: K_transparentWhite2,
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = _duration;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => _audioPlayerState = state);
    });

    _playingRouteState = PlayingRouteState.speakers;
  }

  void _onComplete() {
    setState(() => _playerState = PlayerState.stopped);

    setState(() {
      isPlaying = false;
    });
  }

  Future<int> _stop() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration();
      });
    }
    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1)
      setState(() {
        _playerState = PlayerState.paused;
        isPlaying = false;
      });
    return result;
  }

  Future<int> _play() async {
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(url, position: playPosition);
    if (result == 1)
      setState(() {
        _playerState = PlayerState.playing;
        isPlaying = true;
      });

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Widget slider() {
    return Slider(
      onChangeEnd: (v) {
        if (isPlaying) _audioPlayer.play(url);
      },
      onChangeStart: (V) {
        _audioPlayer.pause();
      },
      onChanged: (v) {
        final Position = v * _duration.inMilliseconds;
        _audioPlayer.seek(Duration(milliseconds: Position.round()));
      },
      activeColor: K_primaryOrange,
      inactiveColor: K_accentOrange,
      value: (_position != null &&
              _duration != null &&
              _position.inMilliseconds > 0 &&
              _position.inMilliseconds < _duration.inMilliseconds)
          ? _position.inMilliseconds / _duration.inMilliseconds
          : 0.0,
    );
  }
}
//
//
//_position=Duration(seconds: 0);
//_stop();
//int length=songsData.length;
//if(length==songsPlayIndex+1){
//setState(() {
//songsPlayIndex=0;
//url =songsData[songsPlayIndex].link128;
//
//});
//_play();
//}
//else{
//setState(() {
//songsPlayIndex++;
//url =songsData[songsPlayIndex].link128;
//});
//_play();
//}
//_position=Duration(seconds: 0);
//_stop();
//int length=songsData.length;
//if(length==songsPlayIndex+1){
//setState(() {
//songsPlayIndex=0;
//url =songsData[songsPlayIndex].link128;
//
//});
//_play();
//}
//else{
//setState(() {
//songsPlayIndex++;
//url =songsData[songsPlayIndex].link128;
//});
//_play();
//}
