import 'dart:io';

import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Pages/Explore/ExplorePage.dart';
import 'package:avapardaz_music/Ui/Pages/HomePage/HomePage.dart';
import 'package:avapardaz_music/Ui/Pages/Library/LibraryPage.dart';
import 'package:avapardaz_music/Ui/Pages/MusicPlayer/MusicPlayer.dart';
import 'package:avapardaz_music/Ui/Pages/Profile/ProfilesPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:avapardaz_music/Ui/Pages/AboutSinger/AboutSinger.dart';

import '../../../main.dart';

class RootPage extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MyStatefulWidget();
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final double _initFabHeight = 120.0;
  double _fabHeight;
  double _panelHeightOpen;
  double _panelHeightClosed = 75.0;
  Color libraryColor;

  double _bodyBottomMargin = 128.0;

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    ExplorePage(),
    LibraryPage(),
    ProfilePage(),
  ];

  PanelController panelController = new PanelController();

  void _onItemTapped(int index) {
    getDataAPI();
    panelController.close();

    setState(() {
      _selectedIndex = index;
      libraryColor = index == 2 ? K_primaryOrange : K_primaryWhite;
    });
  }

  getDataAPI() {
    NetworkUtil.internal().getChannels();
    print(channelsData.length);
    NetworkUtil.internal().getSongs();
    NetworkUtil.internal().getArtist();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, WidgetBuilder> routes = {
      '/': (context) {
        return Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              color: Colors.transparent,
              child: SlidingUpPanel(
                maxHeight: _panelHeightOpen,
                minHeight: _panelHeightClosed,
                renderPanelSheet: true,
                parallaxOffset: .5,
                body: Container(
                    margin: EdgeInsets.only(bottom: _bodyBottomMargin),
                    child: _body()),
                panelBuilder: (sc) => _panel(sc),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(18.0),
                    topRight: Radius.circular(18.0)),
                onPanelSlide: (double pos) => setState(() {
                  _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                      _initFabHeight;
                }),
                collapsed: Container(
                  width: double.infinity,
                  color: K_accentBlack,
                  child: Center(
                      child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          width: 50.0,
                          height: 50.0,
                          margin: EdgeInsets.only(
                              left: 8, right: 16, top: 8, bottom: 8),
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: new NetworkImage(
                                      "https://www.celebrityborn.com/admin/assets/images/people/PVAtfwi05Lhruw04r4Xl_2017_07_24.jpg")))),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            'Dige tablo shode',
                            style: TextStyle(color: K_primaryWhite),
                          ),
                          Text(
                            'fadaei',
                            style: TextStyle(color: K_primaryWhite),
                          ),
                        ],
                      ),
                      Spacer(),
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 16.0, horizontal: 8),
                        width: 32,
                        height: 32,
                        child: Icon(Icons.pause, color: K_primaryWhite),
                      ),
                      Container(
                        margin: EdgeInsets.all(16.0),
                        width: 32,
                        height: 32,
                        child: Icon(
                          Icons.skip_next,
                          color: K_primaryWhite,
                        ),
                      ),
                    ],
                  )),
                ),
              ),
            ),
          ],
        );
      },
      '/two': (context) => AboutSinger()
    };

    _panelHeightOpen = MediaQuery.of(context).size.height * .89;
    final GlobalKey<NavigatorState> navigationKey = GlobalKey<NavigatorState>();

    return WillPopScope(
      onWillPop: () {
        if (panelController.isPanelOpen) {
          panelController.close();
        } else {
          exit(0);
        }
      },
      child: Scaffold(
          backgroundColor: Color(0x272121),
          bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(canvasColor: K_accentBlack),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              showUnselectedLabels: false,
              showSelectedLabels: false,
              unselectedItemColor: K_primaryWhite,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Text('خانه'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.search),
                  title: Text('جستجو'),
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    "assets/images/library.svg",
                    semanticsLabel: 'library',
                    color: libraryColor,
                    height: 16,
                    width: 16,
                  ),
                  title: Text('ترند'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline),
                  title: Text('حساب کاربری'),
                ),
              ],
              backgroundColor: K_accentBlack,
              currentIndex: _selectedIndex,
              selectedItemColor: K_primaryOrange,
              onTap: _onItemTapped,
            ),
          ),
          body: Container(
            color: Colors.transparent,
            child: InkWell(
              highlightColor: Colors.black,
              onTap: () => panelController.open(),
              child: SlidingUpPanel(
                controller: panelController,
                maxHeight: _panelHeightOpen,
                minHeight: _panelHeightClosed,
                renderPanelSheet: true,
                parallaxOffset: .5,
                body: InkWell(
                  highlightColor: Colors.black,
                  onTap: () => null,
                  child: Container(
                      margin: EdgeInsets.only(bottom: _bodyBottomMargin),
                      child: _body()),
                ),
                panelBuilder: (sc) => _panel(sc),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(18.0),
                    topRight: Radius.circular(18.0)),
                onPanelSlide: (double pos) => setState(() {
                  _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                      _initFabHeight;
                }),
                collapsed: Container(
                  width: double.infinity,
                  color: K_accentBlack,
                  child: Center(
                      child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          width: 50.0,
                          height: 50.0,
                          margin: EdgeInsets.only(
                              left: 8, right: 16, top: 8, bottom: 8),
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: new NetworkImage(
                                      "https://www.celebrityborn.com/admin/assets/images/people/PVAtfwi05Lhruw04r4Xl_2017_07_24.jpg")))),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            'Dige tablo shode',
                            style: TextStyle(color: K_primaryWhite),
                          ),
                          Text(
                            'fadaei',
                            style: TextStyle(color: K_primaryWhite),
                          ),
                        ],
                      ),
                      Spacer(),
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 16.0, horizontal: 8),
                        width: 32,
                        height: 32,
                        child: Icon(Icons.pause, color: K_primaryWhite),
                      ),
                      Container(
                        margin: EdgeInsets.all(16.0),
                        width: 32,
                        height: 32,
                        child: Icon(
                          Icons.skip_next,
                          color: K_primaryWhite,
                        ),
                      ),
                    ],
                  )),
                ),
              ),
            ),
          )
          //  WillPopScope(
          //   onWillPop: () {
          //     if (navigationKey.currentState.canPop()) {
          //       navigationKey.currentState.pop();
          //       return Future<bool>.value(false);
          //     }
          //     return Future<bool>.value(true);
          //   },
          //   child: Navigator(
          //       key: navigationKey,
          //       initialRoute: '/',
          //       onGenerateRoute: (RouteSettings routeSettings) {
          //         WidgetBuilder builder = routes[routeSettings.name];
          //         return PageRouteBuilder(
          //           pageBuilder: (context, __, ___) => builder(context),
          //           settings: routeSettings,
          //         );
          //       }),
          // ),
          ),
    );
  }

  @override
  void initState() {
    super.initState();
    _fabHeight = _initFabHeight;
  }

  Widget _body() {
    return Container(width: double.infinity, child: createBottomNav());
  }

  Widget _panel(ScrollController sc) {
    return MusicPlayer(sc);
  }

  Widget createBottomNav() {
    return _widgetOptions.elementAt(_selectedIndex);
  }
}
