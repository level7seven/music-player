import 'package:avapardaz_music/Ui/Components/LibraryChannelsModel.dart';
import 'package:avapardaz_music/Ui/Components/LibrarySongModel.dart';
import 'package:avapardaz_music/Ui/Components/SingerCircle.dart';
import 'package:avapardaz_music/Models/Singer.dart';
import 'package:avapardaz_music/main.dart';
import 'package:flutter/material.dart';

import '../../../Theme/Consts.dart';
import '../../../Theme/sizeConfig.dart';

class ExplorePage extends StatelessWidget {
  const ExplorePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Container(
            color: Color(0xFF282222),
            padding: EdgeInsets.fromLTRB(0, 17, 17, 17),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 17),
                    child: Container(
                      height: 45,
                      decoration: BoxDecoration(
                          color: Color(0xFF353432),
                          borderRadius: BorderRadius.all(Radius.circular(15))),

                      ///Search TextField
                      child: TextField(
                        style: TextStyle(color: Colors.white, fontSize: 13),
                        decoration: new InputDecoration(
                          hintText: "جستجو",
                          hintStyle:
                              TextStyle(color: Colors.white, fontSize: 13),

                          ///TextField Icon
                          ///TODO search
                          icon: IconButton(
                            onPressed: null,
                            icon: Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Icon(
                                Icons.search,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          contentPadding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: <Widget>[
                          ///Singers Row
                          for (var i = 0; i < artistData.length; i++)
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: SingerCircle(
                                    singer: new Singer(
                                        //TODO get from server
                                        biography:
                                            "وی در شهری مذهبی چشم به جهان گشود",
                                        name: artistData[i].name,
                                        coverArt: artistData[i].coverArt)))
                        ],
                      ),
                    ),
                  ),
                  myDivider("جستجوهای اخیر"),

                  ///Recent Songs
                  ///TODO implement component and cache
                  for (var i = 0; i < 4; i++)
                    LibrarySongsModel(
                        circleAvatar: songsData[i].coverArt,
                        title: songsData[i].name,
                        time: "05:02:00", //TODO get song length from server
                        subTitle: songsData[i].singers[0].name),
                  myDivider("کانال های جستجو شده"),

                  ///Recent Channels
                  ///TODO cache
                  for (var i = 0; i < 4; i++)
                    Column(
                      children: [
                        SizedBox(
                          height: 1.46 * SizeConfig.heightMultiplier,
                        ),
                        LibraryChannelsModel(
                          circleAvatar: channelsData[i].circleAvatar,
                          leading: channelsData[i].leading,
                          time: channelsData[i].time,
                          tracksNo: "0",
                          containerColor: channelsData[i].containerColor,
                          textColor: K_primaryWhite,
                        ),
                        SizedBox(
                          height: 0.4 * SizeConfig.heightMultiplier,
                        )
                      ],
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// row diver with [title] on rhe start
  Widget myDivider(
    String title,
  ) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            title,
            style: TextStyle(fontSize: 12, color: Colors.white54),
          ),
        ),
        Expanded(
          child: new Container(
            height: .5,
            margin: EdgeInsets.symmetric(horizontal: 8),
            child: Text(""),
            color: Color(0xff4B4747),
          ),
        ),
      ],
    );
  }
}
