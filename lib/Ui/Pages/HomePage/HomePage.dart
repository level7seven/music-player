import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Models/Geners.dart';

import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Components/Loading.dart';
import 'package:avapardaz_music/Ui/Components/MoreOptionsModel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> carouselSliderPics = List();
  List<Gener> categoryList = List();
 
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: K_primaryBlack,
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: ListView(
            scrollDirection: Axis.vertical,
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    'جااانا',
                    style: TextStyle(color: K_primaryOrange, fontSize: 22),
                  ),
                ),
              ),
              CarouselSlider.builder(
                itemBuilder: (context, index) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                      child: Container(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            margin: EdgeInsets.all(8),
                            width: SizeConfig.widthMultiplier * 16.11,
                            height: 5.12 * SizeConfig.heightMultiplier,
                            decoration: BoxDecoration(
                                color: Color(0xb0272121),
                                borderRadius: BorderRadius.circular(10)),
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 8),
                            child: Text(
                              'Takh  ZedBazi',
                              style: TextStyle(
                                  fontSize: 1.41 * SizeConfig.textMultiplier,
                                  color: K_primaryWhite,
                                  letterSpacing: 0.5),
                            ),
                          ),
                          height: 20.51 * SizeConfig.heightMultiplier,
                          width: MediaQuery.of(context).size.width -
                              8.88 * SizeConfig.widthMultiplier,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            image: DecorationImage(
                                image: Image.asset(carouselSliderPics[index])
                                    .image,
                                fit: BoxFit.fitWidth),
                          )),
                    ),
                  );
                },
                itemCount: carouselSliderPics.length,
                options: CarouselOptions(
                    scrollDirection: Axis.horizontal,
                    height: 25.64 * SizeConfig.heightMultiplier,
                    viewportFraction: 1.0,
                    enlargeCenterPage: true,
                    autoPlay: true,
                    reverse: true),
              ),
              Container(
                width: double.infinity,
                height: 15.12 * SizeConfig.heightMultiplier,
                margin: EdgeInsets.all(8),
                child: ListView.builder(
                  itemCount: genersData.length,
                  reverse: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                        margin: EdgeInsets.all(4),
                        height: 14.10 * SizeConfig.heightMultiplier,
                        width: 14.10 * SizeConfig.heightMultiplier,
                        alignment: Alignment.center,
                        child: Center(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              genersData[index].name,
                              maxLines: 2,
                              style: TextStyle(
                                  color: K_primaryWhite,
                                  fontSize: 2.56 * SizeConfig.textMultiplier,
                                  fontWeight: FontWeight.bold,
                                  shadows: <Shadow>[
                                    Shadow(
                                      offset: Offset(1.0, 1.0),
                                      blurRadius: 3.0,
                                      color: K_primaryBlack,
                                    ),
                                  ]),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: Image.network(
                                genersData[index].coverArt,
                                fit: BoxFit.fill,
                                alignment: Alignment.center,
                              ).image,
                            )));
                  },
                ),
              ),
              Row(children: <Widget>[
                Expanded(
                  child: new Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                      child: Divider(
                        color: K_primaryWhite,
                        height: 4.61 * SizeConfig.heightMultiplier,
                      )),
                ),
                Text(
                  "کانال ها",
                  style: TextStyle(
                      color: K_primaryWhite,
                      fontSize: 2.30 * SizeConfig.textMultiplier),
                ),
                Expanded(
                  child: new Container(
                      margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                      child: Divider(
                        color: K_primaryWhite,
                        height: 4.61 * SizeConfig.heightMultiplier,
                      )),
                ),
              ]),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 32),
                width: 11.11 * SizeConfig.widthMultiplier,
//              height: 300,
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: channelsData.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 16,
                    crossAxisSpacing: 16,
                    childAspectRatio: 1,
//                  childAspectRatio: MediaQuery.of(context).size.width /
//                      (MediaQuery.of(context).size.height / 4),
                  ),
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(2),
                      decoration: BoxDecoration(
                        color: channelsData[index].containerColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Stack(
                        children: [
                          Align(
                            child: Container(
                              child: Text(
                                channelsData[index].leading,
                                style: TextStyle(
                                    color: K_primaryWhite,
                                    fontSize: 2.05 * SizeConfig.textMultiplier),
                              ),
                              margin: EdgeInsets.only(top: 4),
                            ),
                            alignment: Alignment.topCenter,
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: GestureDetector(
                              onTap: (){
                                showModalBottomSheet(
                                    context: context,
                                    builder: (builder){
                                      return MoreOptions();
                                    });
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 4),
                                child: Icon(
                                  Icons.more_vert,
                                  color: K_primaryWhite,
                                  size: 20,
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  ),
                              width: double.infinity,
                              height: 110,
                              margin: EdgeInsets.only(top: 32),
                              child:  ClipRRect(
                                borderRadius:
                                BorderRadius.circular(20),
                                child: CachedNetworkImage(
                                  placeholder: (context, url) => Align(
                                    alignment: Alignment.center,
                                    child: Loading(),
                                  ),
                                  imageUrl:
                                  channelsData[index].circleAvatar,
                                    fit: BoxFit.fill
                                ),
                              ),
                            ),

//                          Image.asset(
//                            categoryList[index].coverArt,
//                            height: 74,
//                            width: double.infinity,
//                            fit: BoxFit.fitWidth,
//                          ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 1.28 * SizeConfig.heightMultiplier,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    makeData();
  }

  void makeData() {
    carouselSliderPics.add('assets/images/pic1 (6).jpeg');
    carouselSliderPics.add('assets/images/pic1 (1).jpeg');
    carouselSliderPics.add('assets/images/pic1 (2).jpeg');
    carouselSliderPics.add('assets/images/pic1 (3).jpeg');

    for (int i = 0; i < 3; i++) {
      categoryList.add(Gener(
          name: 'پاپ',
          coverArt: 'assets/images/pic1 (7).jpeg',
          filterColor: Color(0xff795548)));
      categoryList.add(Gener(
          name: 'راک',
          coverArt: 'assets/images/pic1 (10).jpeg',
          filterColor: Color(0xff2196F3)));
      categoryList.add(Gener(
          name: 'رپ',
          coverArt: 'assets/images/pic1 (11).jpeg',
          filterColor: K_primaryWhite));
      categoryList.add(Gener(
          name: 'نوستالژی',
          coverArt: 'assets/images/pic1 (12).jpeg',
          filterColor: Color(0xffF50057)));
    }
  }
}
