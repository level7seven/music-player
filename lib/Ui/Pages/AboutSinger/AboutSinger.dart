import 'package:avapardaz_music/Ui/Components/LibrarySongModel.dart';
import 'package:avapardaz_music/main.dart';
import 'package:flutter/material.dart';

import '../../../Models/Singer.dart';

class AboutSinger extends StatelessWidget {
  AboutSinger({Key key, @required this.singer}) : super(key: key);
  Singer singer;
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color(0x272121),
      child: SafeArea(
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                      color: Colors.white10,
                      borderRadius: BorderRadius.all(Radius.circular(26))),
                  child: Column(
                    children: <Widget>[
                      /// More Icon on the Top of Card
                      Align(
                        alignment: Alignment.topRight,
                        child: IconButton(
                          icon: Icon(Icons.more_vert, color: Colors.white),
                          onPressed: () {},
                        ),
                      ),

                      ///Artist cover image
                      CircleAvatar(
                        radius: 85,
                        backgroundImage: NetworkImage(singer.coverArt),
                      ),

                      ///Artist name
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          singer.name,
                          style: TextStyle(
                              fontFamily: "IRANYekanMobile",
                              color: Colors.white,
                              fontSize: 20),
                        ),
                      ),

                      ///Artist Biography
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(
                          singer.biography,
                          style: TextStyle(
                              fontFamily: "IRANYekanMobile",
                              color: Colors.white,
                              fontSize: 14),
                        ),
                      ),

                      ///the underline row
                      Padding(
                        padding:
                            const EdgeInsets.only(right: 15, top: 8, left: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            /// Number of songs part
                            Icon(
                              Icons.playlist_play,
                              color: Colors.white,
                              size: 15,
                            ),
                            Text(
                              "35 " + "آهنگ", //TODO get number from server
                              style: TextStyle(
                                fontFamily: "IRANYekanMobile",
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ),

                            /// Number of Albums part
                            Icon(
                              Icons.library_music,
                              color: Colors.white,
                              size: 15,
                            ),
                            Text(
                              "5 " + "آلبوم", //TODO get number from server
                              style: TextStyle(
                                fontFamily: "IRANYekanMobile",
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ),

                            SizedBox(
                              width: 8,
                            ),

                            /// save and favorite part
                            Icon(
                              Icons.save,
                              color: Colors.white,
                              size: 15,
                            ), //TODO use Colors.blue for clicked
                            Icon(
                              Icons.favorite_border,
                              color: Colors.white,
                              size: 15,
                            ), //TODO use Icons.favorite for clicked
                            SizedBox(
                              width: 8,
                            ),

                            /// Follow Button
                            FlatButton(
                                color: Color(0xff2b91e5),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(17.0)),
                                onPressed: () {
                                  print("Follow Singer");
                                }, //TODO send follow request
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 20),
                                  child: Text(
                                    "دنبال کردن",
                                    style: TextStyle(
                                        fontFamily: "IRANYekanMobile",
                                        color: Colors.white,
                                        fontSize: 12),
                                  ),
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                myDivider("آلبوم ها"),
                Container(
                  margin: EdgeInsets.all(8),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemCount: 4,
                    itemBuilder: (context, index) => GridTile(
                      ///Album Component
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  color: Colors.white10,
                                  borderRadius: BorderRadius.circular(10)),

                              ///Album Cover image
                              child: Center(
                                  child: FlutterLogo(
                                //TODO ADD NETWORK IMAGE LATER Album Cover
                                colors: Colors.teal,
                              )),
                            ),
                          ),

                          ///Album name
                          Text(
                            "مجاز",
                            style: TextStyle(
                                fontFamily: "IRANYekanMobile",
                                color: Colors.white54,
                                fontSize: 12),
                          )
                        ],
                      ),
                    ),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                  ),
                ),
                myDivider("آهنگ ها"),
                for (var i = 0; i < 5; i++)
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: LibrarySongsModel(
                        circleAvatar: singer.coverArt,
                        title: songsData[i].name,
                        time: "05:02:00", //TODO get song musics by id
                        subTitle: singer.name),
                  )
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Show a row divider With a [title] between
  Widget myDivider(
    String title,
  ) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              height: .50,
              margin: EdgeInsets.symmetric(horizontal: 8),
              width: 100,
              child: Text(""),
              color: Color(0xff4B4747),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                title,
                style: TextStyle(
                    fontFamily: "IRANYekanMobile",
                    color: Colors.white54,
                    fontSize: 12),
              ),
            ),
            new Container(
              height: .50,
              margin: EdgeInsets.symmetric(horizontal: 8),
              width: 100,
              child: Text(""),
              color: Color(0xff4B4747),
            ),
          ],
        ),
      ),
    );
  }
}
