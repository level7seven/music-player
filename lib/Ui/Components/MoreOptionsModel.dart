import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MoreOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 180.0,
      color: Colors.transparent, //could change this to Color(0xFF737373),
      //so you don't have to change MaterialApp canvasColor
      child: new Container(
        margin: EdgeInsets.all(10),
        decoration: new BoxDecoration(
          color: Color(0xff3B3B3B),
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(30),
            topRight: const Radius.circular(30),
            bottomLeft: const Radius.circular(30),
            bottomRight: const Radius.circular(30),
          ),
        ),
        child: Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: new Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20,right: 13),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/share.svg',
                          semanticsLabel: 'minus',
                          color: K_transparentWhite,
                          height: 16,
                          width: 16,
                        ),
                        SizedBox(width: 10,),
                        Text('ذخیره برای بعدا',style: TextStyle(color: Color(0xffF6E9E9),fontSize: 16),)
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30,right: 13),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/favorite.svg',
                          semanticsLabel: 'minus',
                          color: K_transparentWhite,
                          height: 16,
                          width: 16,
                        ),
                        SizedBox(width: 10,),
                        Text('افزودن به مورد علاقه ها',style: TextStyle(color: Color(0xffF6E9E9),fontSize: 16),)
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30,right: 13),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/share.svg',
                          semanticsLabel: 'minus',
                          color: K_transparentWhite,
                          height: 16,
                          width: 16,
                        ),
                        SizedBox(width: 10,),
                        Text('اشتراک گذاری',style: TextStyle(color: Color(0xffF6E9E9),fontSize: 16),)
                      ],
                    ),
                  ),
                ],
              ),
            )
        ),
      ),
    );
  }
}