//import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Components/Loading.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LibraryChannelsModel extends StatelessWidget {
  final String circleAvatar;
  final String leading;
  final String tracksNo;
  final String time;
  final Color containerColor;
  final Color textColor;
  LibraryChannelsModel(
      {Key key,
      this.circleAvatar,
      this.leading,
      this.tracksNo,
      this.time,
      this.containerColor,
      this.textColor});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Container(
        height: 12.82 * SizeConfig.heightMultiplier,
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            color: containerColor),
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: new Row(
          children: [
            new Padding(
                padding: EdgeInsets.all(3),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(27),
                  child: CachedNetworkImage(
                    imageUrl: circleAvatar,
                    placeholder: (context, url) => Align(
                      child: Loading(),
                      alignment: Alignment.center,
                    ),
                    height: 11.53 * SizeConfig.heightMultiplier,
                    width: 11.53 * SizeConfig.heightMultiplier,
                    fit: BoxFit.fill,
                  ),
                )),
            Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 0, top: 5),
                        child: new Text(
                          leading,
                          style: new TextStyle(
                              fontFamily: "IRANYekanMobile",
                              fontSize: 2.05 * SizeConfig.textMultiplier,
                              color: Colors.white,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      new SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: new Row(
                          children: [
                            SvgPicture.asset(
                              'assets/images/playlist.svg',
                              semanticsLabel: 'playList',
                              color: textColor,
                              height: 1.66 * SizeConfig.heightMultiplier,
                              width: 1.66 * SizeConfig.heightMultiplier,
                            ),
                            new Padding(
                                padding: EdgeInsets.only(right: 5, left: 3),
                                child: new Text(
                                  tracksNo,
                                  style: new TextStyle(
                                    fontSize: 1.53 * SizeConfig.textMultiplier,
                                    color: Colors.white,
                                    fontFamily: "IRANYekanMobile",
                                  ),
                                )),
                            new Text(
                              'آهنگ',
                              style: new TextStyle(
                                fontFamily: "IRANYekanMobile",
                                fontSize: 1.53 * SizeConfig.textMultiplier,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            SvgPicture.asset(
                              'assets/images/Time.svg',
                              semanticsLabel: 'playList',
                              color: textColor,
                              height: 1.66 * SizeConfig.heightMultiplier,
                              width: 1.66 * SizeConfig.heightMultiplier,
                            ),
                            new Padding(
                              padding: EdgeInsets.only(right: 5),
                              child: new Text(
                                time,
                                style: new TextStyle(
                                  color: Colors.white,
                                  fontFamily: "IRANYekanMobile",
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(
                left: 10,
              ),
              child: new Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
