import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LibraryArtistModel extends StatelessWidget {
  final String circleAvatar;
  final String leading;
  final String tracksNo;
  final String albumsNo;

  LibraryArtistModel(
      {Key key, this.circleAvatar, this.leading, this.tracksNo, this.albumsNo});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Container(
        height: 10.24 * SizeConfig.heightMultiplier,
        decoration: new BoxDecoration(
            border: Border.all(color: Color(0xff4B4747)),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: K_accentBlack),
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new Padding(
                  padding: EdgeInsets.only(
                      right: 1.42 * SizeConfig.widthMultiplier, left: 15),
                  child: new CircleAvatar(
                    radius: 25,
                    backgroundImage: new NetworkImage(circleAvatar),
                  ),
                ),
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 0 * SizeConfig.widthMultiplier,
                            top: 1.7 * SizeConfig.heightMultiplier,
                            bottom: 1.7 * SizeConfig.heightMultiplier),
                        child: new Text(
                          leading,
                          style: new TextStyle(
                              fontFamily: "IRANYekanMobile",
                              fontSize: 2.34 * SizeConfig.textMultiplier,
                              color: K_primaryWhite,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      new SizedBox(
                        width: 10,
                      )
                    ]),
//                    new SizedBox(
//                      height: 1.46 * SizeConfig.heightMultiplier,
//                    ),
                    new Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/playlist.svg',
                          semanticsLabel: 'playList',
                          color: K_primaryWhite,
                          height: 1.66 * SizeConfig.heightMultiplier,
                          width: 1.66 * SizeConfig.heightMultiplier,
                        ),
                        new Padding(
                            padding: EdgeInsets.only(
                                right: 1.21 * SizeConfig.widthMultiplier),
                            child: new Text(tracksNo,
                                style: new TextStyle(
                                  fontFamily: "IRANYekanMobile",
                                  fontSize: 1.75 * SizeConfig.textMultiplier,
                                  color: K_primaryWhite,
                                ))),
                        new Text(
                          ' آهنگ',
                          style: new TextStyle(
                            fontFamily: "IRANYekanMobile",
                            fontSize: 1.75 * SizeConfig.textMultiplier,
                            color: K_primaryWhite,
                          ),
                        ),
                        SizedBox(
                          width: 4.86 * SizeConfig.widthMultiplier,
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 10),
                          child: SvgPicture.asset(
                            'assets/images/album.svg',
                            semanticsLabel: 'playList',
                            color: K_primaryWhite,
                            height: 1.66 * SizeConfig.heightMultiplier,
                            width: 1.66 * SizeConfig.heightMultiplier,
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(
                              right: 1.21 * SizeConfig.widthMultiplier),
                          child: new Text(
                            albumsNo,
                            style: new TextStyle(
                              color: K_primaryWhite,
                            ),
                          ),
                        ),
                        new Text(
                          ' آلبوم',
                          style: new TextStyle(
                            fontFamily: "IRANYekanMobile",
                            fontSize: 1.75 * SizeConfig.textMultiplier,
                            color: K_primaryWhite,
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
            new Padding(
              padding: EdgeInsets.only(
                left: 3,
              ),
              child: new Icon(
                Icons.more_vert,
                color: K_primaryWhite,
              ),
            )
          ],
        ),
      ),
    );
  }
}

List<LibraryArtistModel> dummyData2 = [
  LibraryArtistModel(
    circleAvatar: "assets/images/guitar.svg",
    leading: "علی سورنا",
    tracksNo: "35",
    albumsNo: "5",
  ),
  LibraryArtistModel(
    circleAvatar: "assets/images/guitar.svg",
    leading: "علی سورنا",
    tracksNo: "35",
    albumsNo: "5",
  ),
  LibraryArtistModel(
    circleAvatar: "assets/images/guitar.svg",
    leading: "علی سورنا",
    tracksNo: "35",
    albumsNo: "5",
  ),
  LibraryArtistModel(
    circleAvatar: "assets/images/guitar.svg",
    leading: "علی سورنا",
    tracksNo: "35",
    albumsNo: "5",
  ),
];
