import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class LibrarySongsModel extends StatelessWidget {
  final String circleAvatar;
  final String title;
  final String time;
  final String subTitle;

  const LibrarySongsModel(
      {@required this.circleAvatar,
      @required this.title,
      @required this.time,
      @required this.subTitle});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        new ListTile(
          leading: new CircleAvatar(
            radius: 25,
            backgroundImage: new NetworkImage(circleAvatar),
            backgroundColor: Colors.grey,
          ),
          title: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 48.66 * SizeConfig.widthMultiplier,
                child: new Text(
                  title,
                  style: new TextStyle(
                      fontFamily: "IRANYekanMobile",
                      fontSize: 2.34 * SizeConfig.textMultiplier,
                      color: K_primaryWhite,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                        padding: EdgeInsets.only(
                            left: 5, top: 1.46 * SizeConfig.heightMultiplier),
                        child: new Text(
                          "22:30",
                          style: new TextStyle(
                              fontFamily: "IRANYekanMobile",
                              fontSize: 1.75 * SizeConfig.textMultiplier,
                              color: Color(0xffF6E9E9),
                              fontWeight: FontWeight.w400),
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 1.46 * SizeConfig.heightMultiplier,
                      ),
                      child: new Icon(
                        Icons.more_vert,
                        color: K_primaryWhite,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          subtitle: new Padding(
            padding: EdgeInsets.only(top: 0),
            child: new Text(
              subTitle,
              style: new TextStyle(
                  fontFamily: "IRANYekanMobile",
                  fontSize: 1.79 * SizeConfig.textMultiplier,
                  color: Color(0xffF6E9E9),
                  fontWeight: FontWeight.w300),
            ),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(right: 38),
          height: 0.5,
          width: 77.85 * SizeConfig.widthMultiplier,
          color: Color(0xff4B4747),
        ),
      ],
    );
  }
}
