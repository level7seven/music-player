import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(accentColor: K_accentBlack),
      child: new CircularProgressIndicator(
        backgroundColor: K_primaryOrange,
      ),
    );
  }
}
