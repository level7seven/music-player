import 'package:avapardaz_music/Models/Singer.dart';
import 'package:avapardaz_music/Ui/Pages/AboutSinger/AboutSinger.dart';
import 'package:flutter/material.dart';

class SingerCircle extends StatelessWidget {
  SingerCircle({Key key, @required this.singer}) : super(key: key);
  Singer singer;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          ///to be clickable
          InkWell(
            ///Singer cover image
            child: CircleAvatar(
              radius: 32,
              backgroundImage: NetworkImage(singer.coverArt),
            ),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return AboutSinger(
                  singer: singer,
                );
              }));
            },
          ),

          ///Singer name
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                singer.name,
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
