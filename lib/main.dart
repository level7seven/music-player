import 'package:avapardaz_music/Ui/Components/LibraryChannelsModel.dart';
import 'package:avapardaz_music/Models/Geners.dart';
import 'package:avapardaz_music/Models/Singer.dart';
import 'package:avapardaz_music/Models/Song.dart';
import 'package:avapardaz_music/Models/Artist.dart';
import 'package:avapardaz_music/Ui/Pages/SplashScreen/SplashPage.dart';
import 'package:avapardaz_music/Ui/Pages/Root/RootPage.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:flutter/material.dart';

import 'Ui/Components/LibrarySongModel.dart';

void main() {
  runApp(MyApp());

//      MyApp());
}
//*************************************************************************** */
// ! Models Are here

String accessToken = '';
List<LibraryChannelsModel> channelsData = List<LibraryChannelsModel>();
List<Song> songsData = List<Song>();
List<Artist> artistData = new List<Artist>();
List<Gener> genersData = new List<Gener>();

// ! Done!
//************************************************************************* */
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              initialRoute: '/',
              theme: ThemeData(fontFamily: 'IRANYekanMobile',canvasColor: Colors.transparent, ),
              routes: {
                // When navigating to the "/" route, build the FirstScreen widget.
                '/': (context) => SplashPage(),
                // When navigating to the "/second" route, build the SecondScreen widget.
                '/root': (context) => RootPage()
              },
            );
          },
        );
      },
    );
  }
}
