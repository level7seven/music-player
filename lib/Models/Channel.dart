import 'dart:convert';

import 'package:flutter/material.dart';

class Channel {
  String name;
  int songsCount; // not im..
  Duration songsLength; // not implenet in server side
  Color bgColor;
  Color txtColor;
  String imgUrl;

  Channel(
      {@required this.name,
      @required this.songsCount,
      @required this.songsLength,
      @required this.bgColor,
      @required this.txtColor,
      @required this.imgUrl});
  String get channelName => name;
  int get channelSongsCount => songsCount;
  Duration get channelSongsLength => songsLength;
  Color get channelBackgroundColor => bgColor;
  Color get channelTextColor => txtColor;
  String get channelImage => imgUrl;

  factory Channel.fromJson(String str) => Channel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Channel.fromMap(Map<String, dynamic> json) => Channel(
      name: json["name"] == null ? "" : json["name"],
      imgUrl: json["coverArt"] == null ? "" : json["coverArt"],
      bgColor:
          json["bgColor"] == null ? Colors.white : fromHex(json["bgColor"]),
      txtColor: json["textColor"] == null
          ? Colors.black
          : fromHex(json['textColor']));

  Map<String, dynamic> toMap() => {
        "name": name,
        "coverArt": imgUrl,
        "bgColor": '#${bgColor.value.toRadixString(16)}',
        "textColor": '#${txtColor.value.toRadixString(16)}',
      };

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
