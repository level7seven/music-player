import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Artist{
  String id;
  String name;
  String coverArt;
  int songsCount;

  Artist({@required this.id,@required this.name,@required this.coverArt,@required this.songsCount});
  
}