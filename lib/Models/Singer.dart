import 'dart:convert';

class Singer {
  String id;
  String name;
  String coverArt;
  String biography;
  List<String> singerFor;
  String createdAt;
  Singer(
      {this.id,
      this.name,
      this.coverArt,
      this.biography,
      this.singerFor,
      this.createdAt});
  String get singerImage => coverArt;
  String get singerName => name;

  factory Singer.fromJson(String str) => Singer.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Singer.fromMap(Map<String, dynamic> json) => Singer(
        name: json["name"] == null ? "" : json["name"],
        coverArt: json["coverArt"] == null ? "" : json["coverArt"],
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "coverArt": coverArt,
      };
}
