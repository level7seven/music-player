import 'dart:convert';

import 'package:flutter/material.dart';

class Gener {
  String name;
  String id;
  String coverArt;
  Color filterColor;
  Color textColor;
  String createdAt;
  List<String> musics;

  Gener(
      {@required this.name,
      @required this.filterColor,
      @required this.textColor,
      @required this.coverArt,
      @required this.id}
      );
  String get generName => name;
  //int get generSongsCount => songsCount;
  //Duration get generSongsLength => songsLength;
  Color get generfilterColor => filterColor;
  Color get generTextColor => textColor;
  String get generImage => coverArt;

  factory Gener.fromJson(String str) => Gener.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Gener.fromMap(Map<String, dynamic> json) => Gener(
      name: json["name"] == null ? "" : json["name"],
      coverArt: json["coverArt"] == null ? "" : json["coverArt"],
      filterColor: json["filterColor"] == null
          ? Colors.white10
          : fromHex(json["filterColor"]),
      textColor: json["textColor"] == null
          ? Colors.white
          : fromHex(json["textColor"]));

  Map<String, dynamic> toMap() => {
        "name": name,
        "coverArt": coverArt,
        "filterColor": '#${filterColor.value.toRadixString(16)}',
        "textColor": '#${textColor.value.toRadixString(16)}',
      };
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
