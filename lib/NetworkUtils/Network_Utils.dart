import 'dart:convert';

import 'package:avapardaz_music/Ui/Components/LibraryChannelsModel.dart';
import 'package:avapardaz_music/Models/Artist.dart';
import 'package:avapardaz_music/Models/Geners.dart';
import 'package:avapardaz_music/Models/Singer.dart';
import 'package:avapardaz_music/Models/Song.dart';
import 'package:avapardaz_music/main.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../main.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  Future<bool> getChannels() async {
    Client client = Client();
    Uri uri = Uri.http("api.arvincms.ir", "/api/channels");
    Response response = await client.get(uri);

    List<LibraryChannelsModel> channelModel = new List<LibraryChannelsModel>();
    for (var item in json.decode(response.body)) {
      channelModel.add(LibraryChannelsModel(
        leading: item['name'],
        circleAvatar: item['coverArt'],
        containerColor:
            Color(int.parse(item['bgColor'].replaceAll("#", "ff"), radix: 16)),
        time: "00:00:00",
      ));
    }

    channelsData = channelModel;
  }

  Future<bool> getSongs() async {
    Client client = Client();
    Uri uri = Uri.http("api.arvincms.ir", "/api/musics");
    Response response = await client.get(uri);

    // print(json.decode(response.body));
    List<Song> songsModel = new List<Song>();
    for (var item in json.decode(response.body)) {
      songsModel.add(Song(
        name: item['name'],
        coverArt: item['coverArt'],
        releaseDate: item['releaseDate'],
        link128: item['link128'],
        singers: (item['singer'] as List)
            .map((e) => new Singer(
                  name: e['name'],
                  id: e['_id'],
                ))
            .toList(),
      ));
    }
    songsData = songsModel;
    if (response.statusCode == 201) {
      print('true');
      return true;
    } else {
      print('false');
      return false;
    }
  }

  Future<bool> getArtist() async {
    Client client = Client();
    Uri uri = Uri.http("api.arvincms.ir", "api/artists");
    Response response = await client.get(uri);

    // print(json.decode(response.body));
    List<Artist> artistModel = new List<Artist>();
    for (var item in json.decode(response.body)) {
      artistModel.add(Artist(
          name: item['name'],
          coverArt: item['coverArt'],
          id: item['_id'],
          songsCount: item['songs']));
    }
    artistData = artistModel;
    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<int> getChannelsMusic(String channelID) async {
    Client client = Client();

    Map<String, String> params = {
      '_id': channelID,
    };

    Uri uri = Uri.https("sport-club.liara.run", "/api/channel/musics", params);
    Response respons = await client.post(uri);
    List<dynamic> channelsSong = new List<dynamic>();
    for (var item in json.decode(respons.body)) {
      channelsSong.add(item);
    }
    return channelsSong.length;
  }

  Future<bool> getGenres() async {
    Client client = Client();
    Uri uri = Uri.http("api.arvincms.ir", "/api/genres");
    Response response = await client.get(uri);

    // print(json.decode(response.body));
    List<Gener> GenersModel = new List<Gener>();
    for (var item in json.decode(response.body)) {
      GenersModel.add(Gener(
        name: item['name'],
        coverArt: item['coverArt'],
        filterColor: Color(
            int.parse(item['filterColor'].replaceAll("#", "ff"), radix: 16)),
        //textColor: item['textColor'],
        id: item['_id'],
      ));
    }
    genersData = GenersModel;
    if (response.statusCode == 201) {
      print('true');
      return true;
    } else {
      print('false');
      return false;
    }
  }
}
