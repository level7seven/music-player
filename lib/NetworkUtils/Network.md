## Network Utils Class

> This is a singleton class that provide all api request that we use in our app

for each models that we want to get their model there is a future that use http request to get data from server

## Implement a request
1. we define a Client 
2. send a http request to a specific URL
3. then we get a response from server that is in `Json format`
4. we need to decode the json format into a ist and get the body of the response we got from server a use in our app